export function thousandsFormat(num:number) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
}

export function floatComma(num:number){
  return num.toString().replace(/\./g, ',')
}