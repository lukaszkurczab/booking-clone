import React, { useState, useRef } from "react";

import "./style.css";

interface CarouselItemInterface {
  children?: any,
  width?: string,
  space?: string
}

export const CarouselItem = ({ children, width, space }: CarouselItemInterface) => {
  return (
    <div className="carousel-item" style={{ width: width }}  >
      <div
        className="carousel-item-content"
        style={{ marginLeft: space, marginRight: space }}
      >
        {children}
      </div>
    </div >
  );
};

interface CarouselInterface {
  children: any,
  division: number,
  space: number,
  buttonLeft: any,
  buttonRight: any,
  className: string,
  additionalTiles: any
}

const Carousel = ({ children, division, space, buttonLeft, buttonRight, className, additionalTiles }: CarouselInterface) => {
  const [activeIndex, setActiveIndex] = useState<number>(0);
  const ref = useRef<any>(null);
  let maxIndex: number = 0;

  const reduceMaxIndex = () => additionalTiles === undefined ? maxIndex = -1 : null

  reduceMaxIndex()

  const updateIndex = (newIndex: number) => {
    if (newIndex < 0) {
      newIndex = activeIndex;
    } else if (newIndex >= React.Children.count(children)) {
      newIndex = activeIndex;
    } else if (newIndex >= maxIndex - (division - 1)) {
      newIndex = activeIndex;
    }

    setActiveIndex(newIndex);
  };

  return (
    <div>
      <div className={`carousel ${className}`}>
        <div
          className="inner"
          style={{ transform: `translateX(-${(activeIndex * 100) / division}%)` }}
          ref={ref}
        >
          {React.Children.map(children, (child: any) => {
            maxIndex = maxIndex + 1;
            return (
              React.cloneElement(child, {
                width: `${100 / division}%`,
                space: `${space / 2}px`,
              })
            )
          })}
        </div>

      </div>
      <div>
        <button
          onClick={() => {
            updateIndex(activeIndex - 1);
          }}
          style={activeIndex == 0 ? { display: "none" } : { display: "block" }}
        >
          {buttonLeft}
        </button>
        <button
          onClick={() => {
            updateIndex(activeIndex + 1);
          }}
          style={activeIndex == maxIndex - (division) ? { display: "none" } : { display: "block" }}
        >
          {buttonRight}
        </button>
      </div>
    </div>
  );
};

export default Carousel;
