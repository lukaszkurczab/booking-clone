import * as React from 'react'

interface TileInterface {
  title: string,
  desc: string,
  img: string,
  index: number
}

const Tile = ({ title, desc, img, index }: TileInterface) => {
  return (
    <div className={`promotionTile element-${index}`}>
      <h2 className="promotionTile__title">
        {title}
        <img src="./images/flag.png" alt="Polska" className="promotionTile__title-flag" />
      </h2>
      <h3 className="promotionTile__desc">{desc}</h3>
      <img src={img} className="promotionTile__img" />
    </div>
  )
}

export default Tile