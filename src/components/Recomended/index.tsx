import * as React from 'react'
import Tile from './tile'
import { useStaticQuery, graphql } from 'gatsby'

const data = graphql`
    {    
      gcms{
        recommendeds{
          title
          description
          image{
            id
            handle
            fileName
            mimeType
            url
          }
        }
      }
    }
  `

const Recomended = () => {
  const RecomendedQuery = useStaticQuery(data)

  return (
    <div className="promotion__wrapper page-container">
      {
        RecomendedQuery.gcms.recommendeds.map((recommended: any, index: number) => {
          return (
            <Tile
              key={index}
              img={recommended.image.url}
              title={recommended.title}
              desc={recommended.description}
              index={index + 1}
            />
          )
        })
      }
    </div>
  )
}

export default Recomended