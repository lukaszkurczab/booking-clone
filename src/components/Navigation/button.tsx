import * as React from 'react'

interface ButtonInterface {
  name: string,
  children: any,
  id: number
}

const Button = ({ name, children, id }: ButtonInterface) => {
  return (
    <li className={id === 1 ? 'button__wrapper selected' : "button__wrapper"}>
      {children}
      <span className="nav__text" >{name}</span>
    </li>
  )
}
export default Button