import * as React from 'react'
import Button from './button'

import { Bed, Plane, Car, Atractions, Taxi } from '../../assets/icons'

const navigations = [
  {
    'id': 1,
    'name': 'Pobyty',
    'icon': Bed
  },
  {
    'id': 2,
    'name': 'Loty',
    'icon': Plane
  },
  {
    'id': 3,
    'name': 'Wynajem samochodów',
    'icon': Car
  },
  {
    'id': 4,
    'name': 'Atrakcje',
    'icon': Atractions
  },
  {
    'id': 5,
    'name': 'Taksówki lotniskowe',
    'icon': Taxi
  },
]

const Navigation = () => {

  return (
    <nav className="navigation__wrapper">
      <ul className="navigation__list page-container">
        {navigations.map(navItem => (
          <Button key={navItem.id} name={navItem.name} id={navItem.id}>
            <navItem.icon />
          </Button>
        ))}
      </ul>
    </nav>
  )
}

export default Navigation