import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Tiles from '../Carousel'

const data = graphql`
    {    
      gcms{
        communities{
          title
          description
          number
          image{
            id
            handle
            fileName
            mimeType
            url
          }
        }
      }
    }
  `

const Community = () => {
  
  const CommunitiessQuery = useStaticQuery(data)

  return (
    <div className="community">
      <Tiles
        title="Nawiąż kontakt z innymi podróżującymi"
        query={CommunitiessQuery.gcms.communities}
        elements={4}
        modifications="community"
      />
    </div >
  )
}

export default Community