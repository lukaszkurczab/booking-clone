import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { useState } from 'react'

const data = graphql`
    {    
      gcms{
        discovers{
          title
          description
          image{
            id
            handle
            fileName
            mimeType
            url
          }
        }
      }
    }
  `

const isBrowser = typeof window !== "undefined"

const Discover = () => {
  const DiscoversQuery = useStaticQuery(data)
  const discoverQueryLength = DiscoversQuery.gcms.discovers.length

  const navigation = []
  const tiles = []
  const [activeTiles, setActiveTiles] = useState<number>(1)
  const [tileAtOnce, setTileAtOnce] = useState<number>(isBrowser ? (window.innerWidth > 1100 ? 5 : 4) : 5)

  React.useEffect((): any => {
    function handleResize() {
      setTileAtOnce(isBrowser ? (window.innerWidth > 1100 ? 5 : 4) : 5)
    }

    isBrowser ? (window.addEventListener('resize', handleResize)) : null

    return () => {
      isBrowser ? (window.removeEventListener('resize', handleResize)) : null
    }
  })

  for (let i = 0; i * tileAtOnce < discoverQueryLength; i++) {
    navigation.push(<li
      className={`discover__nav-item ${activeTiles === i + 1 ? "active" : ""}`}
      onClick={() => setActiveTiles(i + 1)}
      key={i - 1}
    >
      {i + 1}
    </li>)
  }

  for (let i = tileAtOnce * activeTiles; i > tileAtOnce * activeTiles - tileAtOnce; i--) {
    tiles.push(<div
      className="discover__tile"
      key={i - 1}
    >
      <div className="discover__tile-imgWrapper">
        <img className="discover__tile-img" src={DiscoversQuery.gcms.discovers[i - 1].image.url} />
        <span className="discover__tile-desc">{DiscoversQuery.gcms.discovers[i - 1].description}</span>
      </div>
      <span className="discover__tile-name">{DiscoversQuery.gcms.discovers[i - 1].title}</span>

    </div>)
  }

  return (
    <div className="discover page-container">
      <h2 className="heading">Odkrywaj</h2>
      <div className="discover__nav">
        <ul className="discover__nav-items">
          {navigation}
        </ul>
        <span className="discover__nav-more">Więcej krajów</span>
      </div>
      <div className="discover__tiles">
        {tiles.reverse()}
      </div>
    </div>
  )
}

export default Discover