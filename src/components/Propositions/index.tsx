import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Tiles from '../Carousel'

const data = graphql`
    {    
      gcms{
        propositions{
          title
          description
          cost
          rating
          opinions
          image{
            id
            handle
            fileName
            mimeType
            url
          }
        }
      }
    }
  `

const Propositions = () => {
  const PropositionsQuery = useStaticQuery(data)

  return (
    <div className="propositions">
      <Tiles
        title="Domy, które goście kochają"
        query={PropositionsQuery.gcms.propositions}
        elements={4}
        modifications="propositions"
      >
        <div className="propositions__more">
          <img src="images/placeholder.jpg" alt="" className="propositions__more-background" />
          <p className="propositions__more-text" >Mamy o wiele więcej domów i apartamentów, które Ci się spodobają!</p>
          <button className="propositions__more-button">Zobacz ofertę domów</button>
        </div>
      </Tiles>
    </div>
  )
}

export default Propositions