import * as React from 'react'
import {Link} from 'gatsby'

interface TileInterface {
  title: string,
  desc: string,
  img: string,
  index: number,
  slug: string
}

const Tile = ({ title, desc, img, index, slug }: TileInterface) => {
  return (
    <Link to={`/articles/${slug}`} className={`element-${index}`}>
      <div className={`promotionTile`}>
        <h2 className="promotionTile__title">
          {title}
        </h2>
        <h3 className="promotionTile__desc">{desc}</h3>
        <img src={img} className="promotionTile__img" />
      </div>
    </Link>

  )
}

export default Tile