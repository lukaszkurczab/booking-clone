import * as React from 'react'
import Tile from './tile'
import { useStaticQuery, graphql } from 'gatsby'

const data = graphql`
    {    
      gcms{
        inspirations{
          title
          description
          slug
          image{
            id
            handle
            fileName
            mimeType
            url
          }
        }
      }
    }
  `

const Inspirations = () => {
  const InspirationsQuery = useStaticQuery(data)

  return (
    <div className="page-container">
      <h2 className="heading">Poszukaj inspiracji na kolejną podróż</h2>
      <div className="promotion__wrapper promotion__wrapper-reverse">
        {
          InspirationsQuery.gcms.inspirations.map((inspiration: any, index: number) => {
            return (
              <Tile
                key={index}
                img={inspiration.image.url}
                title={inspiration.title}
                desc={inspiration.description}
                index={index + 1}
                slug={inspiration.slug}
              />
            )
          })
        }
      </div>
    </div>
  )
}

export default Inspirations