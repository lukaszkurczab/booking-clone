import * as React from 'react'

interface ButtonInterface {
  type: string;
  children?: any
}

const Button = ({ type, children }: ButtonInterface) => {
  return (
    <button className={`button button-${type}`}>
      {children}
    </button>
  )
}
export default Button