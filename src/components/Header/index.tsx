import * as React from 'react'
import ButtonsWrapper from './buttonsWapper'
import Logo from '../../assets/logo'

const Header = ({ }) => {
  return (
    <header>
      <div className="page-container heading">
        <Logo />
        <ButtonsWrapper />
      </div>
    </header>
  )
}
export default Header