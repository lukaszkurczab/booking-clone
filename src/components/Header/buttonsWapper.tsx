import * as React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

import Button from './button'

import Help from '../../assets/help'

const ButtonsWrapper = () => {
  return (
    <div className="heading__buttonsWrapper">
      <div className="heading__buttonsWrapper heading__buttonsWrapper-section">
        <Button type="small">
          PLN
        </Button>
        <Button type="small">
          <StaticImage
            className="heading__icon"
            alt="Clifford, a reddish-brown pitbull, dozing in a bean bag chair"
            src="../../assets/flag.png"
            placeholder="none"
          />
        </Button>
        <Button type="small">
          <Help />
        </Button>
      </div>
      <div className="heading__buttonsWrapper heading__buttonsWrapper-section">
        <Button type="big button-big-reverse">
          Udostępnij obiekt
        </Button>
        <Button type="big">
          Zarejestruj się
        </Button>
        <Button type="big">
          Zaloguj się
        </Button>
      </div>
    </div>
  )
}
export default ButtonsWrapper