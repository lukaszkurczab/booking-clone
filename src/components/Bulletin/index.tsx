import * as React from 'react'

const Bulletin = () => {

  return (
    <>
      <div className=" bulletin bulletin-top">
        <h2 className="bulletin__heading">Oszczędzaj czas i pieniądze!</h2>
        <p className="bulletin__desc">Zaprenumeruj nasz biuletyn, a będziemy przesyłać Ci najlepsze oferty</p>
        <div className="bulletin__form">
          <input type="text" className="bulletin__form-input" placeholder="Adres mailowy" />
          <button className="bulletin__form-button">Zaprenumeruj</button>
        </div>
        <label className="bulletin__checkbox">
          <input type="checkbox" className="bulletin__checkbox-input" id="bulletin" name="bulletin" />
          <span className="bulletin__checkboxStyle-box"></span>
          <span className="bulletin__checkbox-text">Wyślij mi link do pobrania BEZPŁATNEJ aplikacji Booking.com!</span>
        </label>
      </div>
      <div className="bulletin bulletin-bottom">
        <button className="bulletin__share">
          Udostępnij obiekt
        </button>
        <div className="bulletin__links">
          <ul className="bulletin__links-wrapper page-container">
            <li className="bulletin__links-link">Wersja na telefon</li>
            <li className="bulletin__links-link">Twoje konto</li>
            <li className="bulletin__links-link">Wprowadzaj zmiany w rezerwacji online</li>
            <li className="bulletin__links-link">Skontaktuj się z obsułgą klienta</li>
            <li className="bulletin__links-link">Zostań naszym Partnerem Afiliacyjnym</li>
            <li className="bulletin__links-link">Booking.com dla Biznesu</li>
          </ul>
        </div>
      </div>
    </>
  )
}

export default Bulletin