import * as React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

const Footer = () => {

  const lists = [["Kraje", "Regiony", "Miasta", "Dzielnice", "Lotniska", "Hotele", "Ciekawe miejsca"], ["Domy", "Apartamenty", "Ośrodki wypoczynkowe", "Wille", "Hostele", "Obiekty B&B", "Pensjonaty"], ["Wyjątkowe miejsca na pobyt", "Wszystkie cele podróży", "Opinie", "Artykuły", "Społeczności podróżujących", "Oferty sezonowe i wakacyjne"], ["Wypożyczalnia samochodów", "Wyszukiwarka lotów", "Rezerwacja restauracji", "Booking.com dla Biur Podróży"], ["Często zadawane pytania dotyczące koronawirusa (COVID-19)", "O Booking.com", "Skontaktuj się z obsługą klienta", "Centrum pomocy", "Careers", "Zrównoważony rozwój", "Informacje prasowe", "Centrum zasobów bezpieczeństwa", "Relacje z inwestorami", "Ogólne Warunki Handlowe", "Rozstrzyganie sporów", "Zasady współpracy", "Oświadczenie o ochronie prywatności i plikach cookies", "Zarządzaj ustawieniami dotyczącymi plików cookie", "Kontakt dla firm"]]

  return (
    <>
      <div className="footer page-container">
        <div className="footer__list-wrapper">
          {lists.map((list: Array<string>, index: number) => (
            <ul className="footer__list" key={index}>
              <>
                {
                  list.map((listItem: string, index: number) => (
                    <li className="footer__list-item" key={index}>
                      {listItem}
                    </li>
                  ))
                }
              </>
            </ul>
          ))}
        </div>
        <div className="footer__extranetLink">
          <a href="#">Logowanie do Extranetu</a>
        </div>
        <div>
          <p className="footer__copyrights">
            Prawa autorskie © 1996–2021 Booking.com™. Wszelkie prawa zastrzeżone.
          </p>
        </div>
        <div className="footer__text">
          <span>Booking.com jest częścią Booking Holdings Inc. – światowego lidera w internetowej branży turystycznej.</span>
        </div>
        <div className="footer__partners">
          <StaticImage
            className="footer__partners-icon"
            alt="Clifford, a reddish-brown pitbull, dozing in a bean bag chair"
            src="../../assets/booking.png"
            placeholder="none"
          />
          <StaticImage
            className="footer__partners-icon"
            alt="Clifford, a reddish-brown pitbull, dozing in a bean bag chair"
            src="../../assets/priceline.png"
            placeholder="none"
          />
          <StaticImage
            className="footer__partners-icon"
            alt="Clifford, a reddish-brown pitbull, dozing in a bean bag chair"
            src="../../assets/kayak.png"
            placeholder="none"
          />
          <StaticImage
            className="footer__partners-icon"
            alt="Clifford, a reddish-brown pitbull, dozing in a bean bag chair"
            src="../../assets/agada.png"
            placeholder="none"
          />
          <StaticImage
            className="footer__partners-icon"
            alt="Clifford, a reddish-brown pitbull, dozing in a bean bag chair"
            src="../../assets/rentalcars.png"
            placeholder="none"
          />
          <StaticImage
            className="footer__partners-icon"
            alt="Clifford, a reddish-brown pitbull, dozing in a bean bag chair"
            src="../../assets/opentable.png"
            placeholder="none"
          />
        </div>
      </div>
    </>
  )
}

export default Footer