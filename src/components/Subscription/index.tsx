import * as React from 'react'
import { SubscriptionIcon, CloseIcon } from '../../assets/icons'

const Subscription = () => {
  return (
    <div className="page-container subscription">
      <div className="subscription__icon">
        <SubscriptionIcon />
      </div>
      <div className="subscription__text">
        <h2 className="subscription__text-heading">
          Zaprenumeruj biuletyn, aby zobaczyć Ukryte Oferty
          <div className="subscription__text-heading-close">
            <CloseIcon />
          </div>
        </h2>
        <p className="subscription__text-desc">Ceny spadną od razu po zapisaniu się!</p>
        <div className="subscription__text-email">
          <input type="email" name="to" placeholder="Wpisz adres e-mail" className="subscription__text-email-input" />
          <button type="submit" className="subscription__text-email-button">Zarejestruj się!</button>
        </div>
      </div>
    </div>
  )
}

export default Subscription