import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Tiles from '../Carousel'

const data = graphql`
    {    
      gcms{
        objects{
          title
          description
          image{
            id
            handle
            fileName
            mimeType
            url
          }
        }
      }
    }
  `

const ObjectsTypes = () => {
  const ObjectsQuery = useStaticQuery(data)

  return (
    <Tiles
      title="Szukaj według rodzaju obiektu"
      query={ObjectsQuery.gcms.objects}
      elements={5}
      modifications='types'
    />
  )
}

export default ObjectsTypes