import * as React from "react";
import Header from "../Header/";
import Bulletin from "../Bulletin";
import Footer from "../Footer";

interface LayoutInterface{
children: any
}

const Layout = ({children}:LayoutInterface) => {
  return (
    <div>
      <Header />
        {children}
      <Bulletin />
      <Footer />
    </div>
  )
}

export default Layout
