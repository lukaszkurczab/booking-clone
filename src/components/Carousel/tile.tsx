import * as React from 'react'
import { useState, useEffect } from 'react'

interface TileInterface {
  img: any,
  title: string,
  index: number,
  desc?: string
  opinions?: number
  rating?: string | null,
  cost?: string | null,
  modifications?: string
  number?: string|null
  tileLinks?: any
}

const Tile = ({ img, title, desc, opinions, rating, cost, modifications, number, tileLinks, index }: TileInterface) => {
  const [ratingDesc, setRatingDesc] = useState<string | null>(null)

  useEffect(() => {
    if (rating) {
      switch (true) {
        case Number(rating.replace(',', '.')) > 9.0:
          setRatingDesc("Znakomity")
          break
        case Number(rating.replace(',', '.')) > 9.5:
          setRatingDesc("Wyjątkowy")
          break
        default:
          setRatingDesc("Przeciętny")
      }
    }
  }, [])
  return (
    <div className="tile" key={index}>
      {modifications === 'examples' ? (
        <div className="tile-header">
          <img src={img.url} className="tile__img" />
          <span className="tile__title">{title}</span>
          <p className="tile__desc">{desc}</p>
        </div>
      ) : (
        <>
          <img src={img.url} alt="" />
          <span className="tile__title">{title}</span>
          <p className="tile__desc">{desc}</p>
        </>
      )}
      {modifications === 'propositions' ? (
        <>
          <p className="tile__cost"><span>Ceny od {cost} zł</span></p>
          <div className="tile__rating">
            <div className="tile__rating-box">{rating}</div>
            <div>
              <span className="tile__rating-desc">{ratingDesc}</span>
              <span className="tile__rating-opinions">{opinions} opinii</span>
            </div>
          </div>
        </>
      ) : (
        null
      )}
      {modifications === 'community' ? (
        <>
          <p className="tile__number"><span>{number} podróżujących</span></p>
        </>
      ) : (
        null
      )}
      {modifications === 'examples' ? (
        <div className="tile__links">
          {tileLinks.map(((tileLink: string, index: number) => (
            <p className="tile__links-link" key={index}><span>{tileLink}</span></p>
          )))}
        </div>
      ) : (
        null
      )}
    </div>
  )
}

export default Tile