import * as React from 'react'
import Tile from './tile'
import Carousel, { CarouselItem } from '../../utils/Carousel'
import { ArrowIcon } from '../../assets/icons'
import { isBrowser } from '../../utils/IsBrowser';
import {thousandsFormat, floatComma} from '../../utils/Formats'

interface TilesInterface {
  title: string,
  elements: number,
  query: any,
  desc?: string,
  modifications?: string,
  children?: any,
}

const Tiles = ({ ...props }: TilesInterface) => {

  return (
    <div className={`page-container carousel__wrapper carousel__wrapper-${props.modifications}`}>
      <h2 className="heading">{props.title}</h2>
      <h3 className="carousel__desc">{props.desc}</h3>
          <Carousel
            division={props.elements}
            space={16}
            buttonLeft={<div className="carousel__customArrows-button carousel__customArrows-left"><span><ArrowIcon /></span></div>}
            buttonRight={<div className="carousel__customArrows-button carousel__customArrows-right"><span><ArrowIcon /></span></div>}
            className="tiles__wrapper tiles__wrapper"
            additionalTiles={props.children}
          >
            {props.query.map(((tile: any, index:number) => (
              <CarouselItem
                key={index}
              >
                <Tile
                  index={index}
                  img={tile.image}
                  title={tile.title}
                  desc={tile.description}
                  opinions={props.modifications === 'propositions' ? tile.opinions : null}
                  rating={props.modifications === 'propositions' ? floatComma(tile.rating)  : null}
                  cost={props.modifications === 'propositions' ? thousandsFormat(tile.cost) : null}
                  number={props.modifications === 'community' ? thousandsFormat(tile.number) : null}
                  tileLinks={props.modifications === 'examples' ? tile.link : null}
                  modifications={props.modifications}
                />
              </CarouselItem>
            )))}
            <div className="carousel-item carousel-item-add" style={{ width: `${100 / props.elements}%` }}>
              {props.children}
            </div>
          </Carousel>
    </div>
  )
}

export default Tiles