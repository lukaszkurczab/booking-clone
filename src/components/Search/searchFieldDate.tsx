// @ts-nocheck

import * as React from 'react'
import { CalendarIcon } from '../../assets/icons'
import { DateRangePicker } from 'react-date-range';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import * as locales from 'react-date-range/dist/locale';
import { addDays } from 'date-fns';

const isBrowser = typeof window !== "undefined"

const searchFieldDate = () => {
  const [startDay, setStartDay] = React.useState('Zameldowanie')
  const [endDay, setEndDay] = React.useState('Wymeldowanie')
  const [activeCalendar, setActiveCalendar] = React.useState(false)
  const [state, setState] = React.useState([
    {
      startDate: null,
      endDate: null,
      key: 'selection'
    }
  ]);

  if (isBrowser) {
    window.addEventListener('click', (e)=>{
      if(e.target.classList.value.includes("rdr") || e.target.parentNode.classList.value.includes("rdr")){
        setActiveCalendar(true)
      } else {
        setActiveCalendar(false)
      }
    })
  }

  const daysArray =["pon", "wt", "śr", "czw", "pt", "sob", "nd"]
  const monthAttay =["STY", "LUT", "MAR", "KWI", "MAJ", "CZE", "LIP", "SIE", "WRZ", "PAŹ", "LIS", "GRU"]

  React.useEffect(()=>{
    if (state[0].startDate !== null && state[0].endDate !== null){
    setStartDay(`${daysArray[state[0].startDate.getDay()]} ${state[0].startDate.getDate()} ${monthAttay[state[0].startDate.getMonth()]}`)
    setEndDay(`${daysArray[state[0].endDate.getDay()]} ${state[0].endDate.getDate()} ${monthAttay[state[0].endDate.getMonth()]}`)
  }}, [state])

  return (
    <div className="searchField searchField-date rdr">
      <div className="searchField__iconWrapper rdr">
        <CalendarIcon />
      </div>
      <div className="searchField-date-handler rdr">
        <span className="searchField-date-from rdr">{startDay}</span>
        <span className="searchField-date-to rdr">{endDay}</span>
      </div>
      {activeCalendar && (
        <div className={state[0].startDate !== null ? "dateModal rdrWrapper" : "dateModal rdrWrapper disabledCalendar"}>
          <DateRangePicker
            ranges={state}
            onChange={item => setState([item.selection])}
            months={2}
            direction="horizontal"
            showPreview={false}
            showSelectionPreview={false}
            weekStartsOn={1}
            locale={locales['pl']}
            minDate={addDays(new Date(), 0)}
            initialFocusedRange={[0, 100]}
            monthDisplayFormat='LLLL u'
            startDatePlaceholder='Zameldowanie'
          />
          <div className="calendarFooter rdrFooter">
            {startDay} - {endDay} {`${state[0].endDate - state[0].startDate > 0 ? ('(pobyt na ' + (state[0].endDate - state[0].startDate)/(24*60*60*1000) + ' nocy)') : ''}`}
          </div>
        </div>
      )}
    </div>
  )
}

export default searchFieldDate