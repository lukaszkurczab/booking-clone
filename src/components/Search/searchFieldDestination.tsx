import * as React from 'react'
import { BedGrey, Pin } from '../../assets/icons'
import { useStaticQuery, graphql } from 'gatsby'

const isBrowser = typeof window !== "undefined"

const data = graphql`
    {    
      gcms{
        polishLocations{
          title
        }
      }
    }
  `

const searchFieldDestination = () => {
  const [activeDestination, setActiveDestination] = React.useState<boolean>(false)
  const [value, setValue] = React.useState<string>('')
  const hintsQuery = useStaticQuery(data)
  const [hints, setHints] = React.useState<any>(hintsQuery.gcms.polishLocations)

  if (isBrowser) {
    window.addEventListener('click', (e:any)=>{
      if(e.target.classList.contains("searchField-destination-handler")){
        setActiveDestination(true)
      } else {
        setActiveDestination(false)
      }
    })
  }

  const handleChange = (e:any) => {
    e.preventDefault()
    setValue(e.target.value)
    setHints(e.target.value !== '' ? hintsQuery.gcms.polishLocations.filter((location:any) => location.title.toLowerCase().includes(e.target.value.toLowerCase())) : hintsQuery.gcms.polishLocations)
  }
 
  return (
    <div className="searchField searchField-destination">
      <div className="searchField__iconWrapper">
        <BedGrey />
      </div>
      <input 
      type="text" 
      placeholder="Dokąd się wybierasz?"
      className="searchField-destination-handler"
      value={value}
      onChange={(e) => handleChange(e)}
      />
      {activeDestination && (
        <div className="destinationModal"> 
        <h3 className="destinationModal-title">Popularne miejsca w pobliżu</h3>
        {hints.map((hint:any, index:number)=>{while (index <5){return(hint.title.toLowerCase().includes(value.toLowerCase()) ? (
          <div className="destinationModal-element" key={index} onClick={()=>setValue(hint.title)}>
            <Pin/>
            <div className="destinationModal-text">
              <h4 className="destinationModal-textTitle">{hint.title}</h4>
              <span className="destinationModal-textDescription">Polska</span>
            </div>
        </div>) : null
        )}})}
        </div>
      )}
    </div>
  )
}

export default searchFieldDestination