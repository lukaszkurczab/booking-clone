import * as React from 'react'


const Checkbox = () => {

  return (
    <label className="checkbox-wrapper">
      <input type="checkbox" className="checkbox__input" id="buissnesTrip" name="buissnesTrip" />
      <span className="checkboxStyle-box"></span>
      <span className="checkboxText-style">Podróżuję służbowo</span>
    </label>
  )
}

export default Checkbox