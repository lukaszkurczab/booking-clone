import * as React from 'react'
import { Person } from '../../assets/icons'

const isBrowser = typeof window !== "undefined"

const searchFieldParticipants = () => {
  const [activeParticipants, setActiveParticipants] = React.useState<boolean>(false)
  const [adults, setAdults] = React.useState(2)
  const [childrens, setChildrens] = React.useState(0)
  const [rooms, setRooms] = React.useState(1)

  if (isBrowser) {
    window.addEventListener('click', (e:any)=>{
      if(e.target.classList.contains("searchField-participants-handler")
      || e.target.classList.contains("modal-handler")
      ||e.target.parentNode.classList.contains("modal-handler")){
        setActiveParticipants(true)
      } else {
        setActiveParticipants(false)
      }
    })
  }

  return (
    <div className="searchField searchField-participants">
        <div className="searchField__iconWrapper">
          <Person />
        </div>
        <div className="searchField-participants-textWrapper searchField-participants-handler">
          <span className="text searchField-participants-handler">{adults} dorosłych</span>
          <span className="dot"></span>
          <span className="text searchField-participants-handler">{childrens} dzieci</span>
          <span className="dot"></span>
          <span className="text searchField-participants-handler">{rooms} pokój</span>
          <div className="imgWrapper">
            <img
              alt="Zmień ilość"
              src="./images/arrows.svg"
              className="searchField-participants-handler"
            />
          </div>
        </div>

        {activeParticipants && (
        <div className="participantsnModal modal-handler">
          <div className="counter">
            <span className="counter-text modal-handler">Dorośli</span>
            <div className="counter-buttonsWrapper modal-handler">
              <div 
                className={adults === 1 ? "counterbutton disabled" : "counterbutton"} 
                onClick={()=>(adults > 1 ? setAdults(adults - 1) : setAdults(1))}
              >-</div>
              <span>{adults}</span>
              <div 
              className="counterbutton"
              onClick={()=>setAdults(adults + 1)}
              >+</div>
            </div>
          </div>
          <div className="counter modal-handler">
            <div>
              <span className="counter-text modal-handler">Dzieci</span>
              <span className="counter-text-description modal-handler">w wieku od 0 do 17 lat</span>
            </div>
            <div className="counter-buttonsWrapper modal-handler">
              <div 
              className={childrens === 0 ? "counterbutton disabled" : "counterbutton"}
              onClick={()=>(childrens > 0 ? setChildrens(childrens - 1) : setChildrens(0))}
              >-</div>
              <span>{childrens}</span>
              <div
              className="counterbutton modal-handler"
              onClick={()=>setChildrens(childrens + 1)}
              >+</div>
            </div>
          </div>
          <div className="counter modal-handler">
            <span className="counter-text modal-handler">Pokoje</span>
            <div className="counter-buttonsWrapper">
              <div className={rooms === 1 ? "counterbutton disabled" : "counterbutton"}
              onClick={()=>(rooms > 1 ? setRooms(rooms - 1) : setRooms(1))}
              >-</div>
              <span>{rooms}</span>
              <div
              className="counterbutton modal-handler"
              onClick={()=>setRooms(rooms + 1)}
              >+</div>
            </div>
          </div>
        </div>
        )}
      </div>
  )
}

export default searchFieldParticipants