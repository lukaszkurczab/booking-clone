import * as React from 'react'
import SearchFieldDestination from './searchFieldDestination'
import SearchFieldDate from './searchFieldDate'
import SearchFieldParticipants from './searchFieldParticipants'
import Checkbox from './checkbox'

const Search = () => {
  
  return (
    <div className="search__wrapper">
      <div className="search page-container">
        <h2 className="search__heading">
          Znajdź oferty hoteli, domów i wielu innych obiektów...
        </h2>
        <h3 className="search__heading-small">
          Od przytulnych domków wiejskich po modne apartamenty w mieście
        </h3>
        <div className="searchFields__wrapper">
          <SearchFieldDestination/>
          <SearchFieldDate/>
          <SearchFieldParticipants/>
          <button type="button" >
            <input type="submit" className="button" value="szukaj" />
          </button>
        </div>
        <Checkbox/>
      </div>
    </div>
  )
}

export default Search