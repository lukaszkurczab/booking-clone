import * as React from 'react'
import { useState } from 'react'

import { AlertIcon, ArrowIcon } from '../../assets/icons'

const Alert = ({ }) => {
  const [active, setActive] = useState<boolean>(false)

  return (
    <div className="alert__section">
      <div className={active ? "page-container alert__wrapper" : "page-container alert__wrapper active"} onClick={() => setActive(!active)}>
        <div className="alert__heading">
          <span className="icon">
            <AlertIcon />
          </span>
          <span className="text">
            Koronawirus (COVID-19) – wsparcie
            <ArrowIcon />
          </span>
        </div>
        <div className="alert__more">
          <p>Uzyskaj porady dotyczące podróżowania, których potrzebujesz. Dowiedz się więcej o możliwych ograniczeniach, zanim wyruszysz w podróż.</p>
          <a href="#" className="link">Dowiedz się więcej</a>
        </div>
      </div>
    </div>
  )
}
export default Alert