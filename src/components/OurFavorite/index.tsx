import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { useState } from 'react'

const data = graphql`
    {    
      gcms{
        ourPlaces{
          regionName
          regionObjects
          cityName
          cityObjects
          placeName
          placeObjects
        }
      }
    }
  `

const OurFavorite = () => {
  const OurFavoritesQuery = useStaticQuery(data)

  const [tab, setTab] = useState("Regiony")

  return (
    <div className="page-container">
      <h2 className="heading">Nasze ulubione miejsca</h2>
      <div className="ourFavorite__buttons">
        <div className={`ourFavorite__button ${tab === "Regiony" ? "active" : ""}`}
          onClick={() => setTab("Regiony")}>
          Regiony
        </div>
        <div className={`ourFavorite__button ${tab === "Miasta" ? "active" : ""}`}
          onClick={() => setTab("Miasta")}>
          Miasta
        </div>
        <div className={`ourFavorite__button ${tab === "Ciekawe miejsca" ? "active" : ""}`}
          onClick={() => setTab("Ciekawe miejsca")}>
          Ciekawe miejsca
        </div>
      </div>
      <div className="ourFavorite__linksBox" style={{ display: `${tab === "Regiony" ? "block" : "none"}` }}>
        {OurFavoritesQuery.gcms.ourPlaces[0].regionName.map((region: string, index: number) => (
          <p className="ourFavorite__text" key={index}>
            <span className="ourFavorite__text-name">{region}</span>
            <span className="ourFavorite__text-number">{OurFavoritesQuery.gcms.ourPlaces[0].regionObjects[index]}</span>
          </p>))}
      </div>
      <div className="ourFavorite__linksBox" style={{ display: `${tab === "Miasta" ? "block" : "none"}` }}>
        {OurFavoritesQuery.gcms.ourPlaces[0].cityName.map((city: string, index: number) => (
          <p className="ourFavorite__text" key={index}>
            <span className="ourFavorite__text-name">{city}</span>
            <span className="ourFavorite__text-number">{OurFavoritesQuery.gcms.ourPlaces[0].cityObjects[index]}</span>
          </p>))}
      </div>
      <div className="ourFavorite__linksBox" style={{ display: `${tab === "Ciekawe miejsca" ? "block" : "none"}` }}>
        {OurFavoritesQuery.gcms.ourPlaces[0].placeName.map((place: string, index: number) => (
          <p className="ourFavorite__text" key={index}>
            <span className="ourFavorite__text-name">{place}</span>
            <span className="ourFavorite__text-number">{OurFavoritesQuery.gcms.ourPlaces[0].placeObjects[index]}</span>
          </p>))}
      </div>
    </div>
  )
}

export default OurFavorite