import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Tiles from '../Carousel'

const data = graphql`
    {    
      gcms{
        polishLocations{
          title
          description
          image{
            id
            handle
            fileName
            mimeType
            url
          }
        }
      }
    }
  `

const PolishLocationsPage = () => {

  const PolishLocationsQuery = useStaticQuery(data)

  return (
    <Tiles
      title="Polska – odkryj to miejsce"
      desc="Te popularne miejsca mają wiele do zaoferowania"
      query={PolishLocationsQuery.gcms.polishLocations}
      elements={5}
    />
  )
}

export default PolishLocationsPage