import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Tiles from '../Carousel'

const data = graphql`
    {    
      gcms{
        examples{
          title
          description
          link
          image{
            id
            handle
            fileName
            mimeType
            url
          }
        }
      }
    }
  `

const ExampleLocations = () => {

  const ExampleLocationsQuery = useStaticQuery(data)

  return (
    <div className="examples">
      <Tiles
        title=""
        query={ExampleLocationsQuery.gcms.examples}
        elements={4}
        modifications="examples"
      />
    </div>
  )
}

export default ExampleLocations