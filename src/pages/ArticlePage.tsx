import React from "react";
import {graphql} from "gatsby"
import Layout from "../components/Layout";
import Parser from 'html-react-parser';
import '../styles/themes/default/themes.scss'

const ArticlePage = ( {data} ) => {
  const articleData = data.gcms.inspirations[0]

  return(
    <Layout>
      <div className="article-imageWrapper" style={{backgroundImage:`url(${articleData.image.url})`}}/>
      <div className="article page-container">
        <p className="article-path">
          <span className="article-link">Artykuły</span> &gt; <span className="article-link">{articleData.category}</span> &gt; <span className="article-link">{articleData.title}</span>
        </p>
        <h2 className="article-title">{articleData.title}</h2>
        <div className="article-category">{articleData.category}</div>
        <div className="article-introduction">{Parser(articleData.introduction.html)}</div>
        <div className="paragraph">
        {Parser(articleData.mainText.html)}
        </div>
      </div>
    </Layout>
  )
}

export const pageQuery = graphql`
  query ArticlePageQuery{
    gcms{
      inspirations{
        id
        title
        slug
        category
        image{
          id
          handle
          fileName
          mimeType
          url
        }
        introduction{
          html
          markdown
          raw
          text
        }
        mainText{
          html
          markdown
          raw
          text
        }
      }
    }
  }
`

export default ArticlePage