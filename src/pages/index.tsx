import * as React from "react";
import { Helmet } from "react-helmet";
import Alert from "../components/Alert";
import '../styles/themes/default/themes.scss'
import Search from "../components/Search";
import PolishLocationsPage from "../components/PolishLocationsPage";
import ObjectsTypes from "../components/ObjectsTypes";
import Recomended from "../components/Recomended";
import Subscription from "../components/Subscription";
import Inspirations from "../components/Inspirations";
import Propositions from "../components/Propositions";
import Community from "../components/Community";
import ExampleLocations from "../components/ExampleLocations";
import OurFavorite from "../components/OurFavorite";
import Discover from '../components/Discover';
import Navigation from '../components/Navigation'
import Layout from "../components/Layout";

const IndexPage = () => {

  return (
    <>
      <div>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Booking.com | Oficjalna strona | najlepsze hotele i nie tylko</title>
        </Helmet>
        <Layout>
          <Navigation />
          <Alert />
          <Search />
          <PolishLocationsPage />
          <ObjectsTypes />
          <Recomended />
          <Subscription />
          <Inspirations />
          <Propositions />
          <Community />
          <ExampleLocations />
          <OurFavorite />
          <Discover />
        </Layout>
      </div>
    </>
  )
}

export default IndexPage
