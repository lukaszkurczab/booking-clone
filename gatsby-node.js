exports.createPages = async({graphql, actions }) => {
  const {
    data: {
      gcms: {inspirations}
    }
  } = await graphql(`
    {
      gcms{
        inspirations(stage: PUBLISHED){
          id
          title
          slug
          category
          image{
            id
            handle
            fileName
            mimeType
            url
          }
          introduction{
            html
            markdown
            raw
            text
          }
          mainText{
            html
            markdown
            raw
            text
          }
        }
      }
    }
  `)

  inspirations.forEach(({id, slug})=>actions.createPage({
      path:`/articles/${slug}`,
      component: require.resolve(`./src/pages/ArticlePage.tsx`),
      context:{
        id,
      }
    }))
}
